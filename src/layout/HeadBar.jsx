import { AppBar, Toolbar, Typography, Button, Link } from "@mui/material";
import { Box } from "@mui/system";
import { AccountCircle } from '@mui/icons-material';
import ChatIcon from '@mui/icons-material/Chat';

import { UserContext } from '/workspace/src/index'
import { useContext } from "react";

export default function HeadBar() {
    const user = useContext(UserContext)
    return (
        <Box mb={4}>
            <AppBar position="static">
                <Toolbar>
                    <Box mr={2}>
                        <ChatIcon fontSize={'large'} />
                    </Box>
                    <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
                        Simple Chat App
                    </Typography>
                    <AccountCircle />
                    {
                        user ?
                            <Button color="inherit" onClick={() => {
                                localStorage.removeItem('token')
                            }}>
                                Logout
                            </Button> :
                            <Button color="success">
                                <Link href="/login" underline="none">
                                LOGIN
                                </Link>
                            </Button>
                    }                    
                </Toolbar>
            </AppBar>
        </Box>
    )
}