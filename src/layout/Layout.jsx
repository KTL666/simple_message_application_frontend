import { Fragment } from 'react';
import HeadBar from '/workspace/src/layout/HeadBar'
import { Divider } from "@mui/material";

export default function Layout({ children }) {
    return (
        <Fragment>
            <HeadBar />
            <Divider />
            {children}
        </Fragment>
    );
}
