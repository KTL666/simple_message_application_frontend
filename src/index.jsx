import React from 'react';
import ReactDOM from 'react-dom/client';
import { BrowserRouter, Routes, Route } from "react-router-dom";
import SignIn from '/workspace/src/user/SignIn'
import Layout from '/workspace/src/layout/Layout'
import Chatroom from '/workspace/src/chatroom/Chatroom'
import { useContext } from "react";

export const UserContext = React.createContext(undefined)
// const user = useContext(UserContext)

ReactDOM.createRoot(document.getElementById('root')).render(
  <BrowserRouter>
    <Layout>
      <Routes>
        {/* <Route path="/user" > */}
          <Route path="/login" element={<SignIn />} />
          {/* <Route path="logup" element={<LogUp />} /> */}
        {/* </Route> */}
        {/* {user ? <Route path="" element={<Chatroom />} />:null} */}
        <Route path="" element={<Chatroom />} />
      </Routes>
    </Layout>
  </BrowserRouter>
);
