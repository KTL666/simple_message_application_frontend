import { useState, useEffect } from 'react';
import axios from 'axios';

axios.defaults.baseURL = `http://${process.env.REACT_APP_BACKEND_HOST}`;

export default function useAxios ({ url, method }) {
    const [response, setResponse] = useState(undefined);
    const [error, setError] = useState('');
    const [loading, setloading] = useState(false);
    
    axios.interceptors.request.use(
        async (config) => {
            const token = localStorage.getItem("token");
            if (token) {
                config.headers = {
                    authorization: `Bearer ${token}`
                };
            }
            return config;
        },
        (error) => Promise.reject(error)
    );

    const fetchData = (body) => {
        setloading(true);
        axios[method](url, body)
            .then((res) => {
                setResponse(res.data);
            })
            .catch((err) => {
                setError(err);
            })
            .finally(() => {
                setloading(false);
            });
    };

    return [response, error, loading, fetchData ];
};
