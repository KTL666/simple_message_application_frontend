import { List, ListItem, ListItemText } from "@mui/material";
import { Fragment, useEffect, useRef, useState } from "react";

export default function FriendList() {


    const scrollBottomRef = useRef(null);
    const [chatMessages, setChatMessages] = useState([]);

    const listChatMessages = chatMessages.map((chatMessageDto, index) =>
        <ListItem
            key={index}
            sx={{
                overflow: 'auto'
            }}
        >
            <ListItemText primary={`${chatMessageDto.user}: ${chatMessageDto.message}`} />
        </ListItem>
    );

    return (
        <List
            sx={{
                height: '30rem',
            }}
        >
            {listChatMessages}
            <ListItem ref={scrollBottomRef}></ListItem>
        </List>
    );
}