import { Fragment, useEffect, useRef, useState } from "react";
import { List, ListItem, ListItemButton, ListItemText} from '@mui/material/';

export default function ChatroomList({ chatrooms, setChatroom }) {
    useEffect(() => {
        console.log('chatrooms', chatrooms)
    }, [chatrooms])
    return (
        <List
            sx={{
                height: '30rem',
                overflow: 'auto'
            }}
        >
            {
                chatrooms?.map(chatroom => 
                    <ListItemButton
                        // selected={selectedIndex === 0}
                        onClick={(event) => setChatroom(chatroom)}
                        key={chatroom}
                    >
                        <ListItemText primary={chatroom} />
                    </ListItemButton>
                )
            }
        </List>
    );
}