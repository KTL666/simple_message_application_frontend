import { Divider, Typography } from "@mui/material";
import { Fragment } from 'react';

export default function HeadBer({ title }) {
    return (
        <Fragment>
            <Typography variant="h6" gutterBottom>
                { title }
            </Typography>
            <Divider />
        </Fragment>
    )
}