import { Grid, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";
import ChatroomList from '/workspace/src/chatroom/ChatroomList'
import MessageBox from '/workspace/src/chatroom/MessageBox'
import SendBox from '/workspace/src/chatroom/Sendbox'
import FriendList from '/workspace/src/chatroom/FriendList'
import Header from '/workspace/src/chatroom/Header'
import { Fragment, useEffect, useRef, useState } from "react";
import webSocket from 'socket.io-client'
import useAxios from '/workspace/src/utils/useAxios'


export default function Chatroom() {
    // const webSocket = useRef(null);
    const [chatroom, setChatroom] = useState(undefined)
    const [ws, setWs] = useState(webSocket('http://localhost:8080', {
        // withCredentials: true,
        extraHeaders: {
            'Authorization': `Bearer ${localStorage.getItem("token")}`
        }
    }))

    const [chatrooms, chatroomLoading, chatroomError, getChatrooms] = useAxios({
        method: 'get',
        url: '/user/chatroom_list',
    });

    useEffect(() => {
        // ws.onopen = (event) => {
        //     console.log('Open:', event);
        //     getChatroomList()
        // }
        // ws.onclose = (event) => {
        //     console.log('Close:', event);
        // }
        ws.on('message', message => {
            console.log('message', message)
        })
        getChatrooms()
    }, [ws])

    // useEffect(() => {
    //     if (chatrooms) {
    //         console.log('chatroomList', chatrooms, typeof chatrooms)
    //         for(const chatroom of chatrooms) {
    //             ws.on(chatroom, message => {
    //                 console.log(chatroom, message)
    //             })
    //         }
    //     }
    // }, [chatrooms])

    useEffect(() => {
        console.log('listen on', chatroom,)
        ws.on(chatroom, message => {
            console.log(chatroom, message)
        })
    }, [chatroom])


    return (
        <Fragment>
            {/* <Container> */}
                <Paper elevation={5}>
                    <Box p={3}>
                        <Grid container spacing={1} alignItems="center">
                            <Grid item xs={2}>
                                <Header title="Chatrooms" />
                                <ChatroomList chatrooms={chatrooms} setChatroom={setChatroom}/>
                            </Grid>
                            <Grid item xs={8}>
                                <Header title={ chatroom?chatroom:"Please Select A Chatroom" } />
                                <MessageBox ws={ws} />
                                <br />
                                <SendBox ws={ws} chatroom={chatroom} />
                            </Grid>
                            <Grid item xs={2}>
                                <Header title="Online Friends" />
                                <FriendList ws={ws} />
                            </Grid>
                        </Grid>
                    </Box>
                </Paper>
            {/* </Container> */}
        </Fragment>
    );
}
