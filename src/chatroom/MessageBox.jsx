import {  List, ListItem, ListItemText } from "@mui/material";
import { Fragment, useEffect, useRef, useState } from "react";

export default function MessageBox() {

    // const ENTER_KEY_CODE = 13;

    const scrollBottomRef = useRef(null);
    
    const [chatMessages, setChatMessages] = useState([]);
    // const [user, setUser] = useState('');
    

    

    // useEffect(() => {
    //     webSocket.current.onmessage = (event) => {
    //         const chatMessageDto = JSON.parse(event.data);
    //         console.log('Message:', chatMessageDto);
    //         setChatMessages([...chatMessages, {
    //             user: chatMessageDto.user,
    //             message: chatMessageDto.message
    //         }]);
    //         if (scrollBottomRef.current) {
    //             scrollBottomRef.current.scrollIntoView({ behavior: 'smooth' });
    //         }
    //     }
    // }, [chatMessages]);




    

    const listChatMessages = chatMessages.map((chatMessageDto, index) =>
        <ListItem
            key={index}
            sx={{
                // height: '25rem',
                overflow: 'auto'
            }}
        >
            <ListItemText primary={`${chatMessageDto.user}: ${chatMessageDto.message}`} />
        </ListItem>
    );

    return (
        <List
            sx={{
                height: '24rem',
            }}
        >
            {listChatMessages}
            <ListItem ref={scrollBottomRef}></ListItem>
        </List>
    );
}