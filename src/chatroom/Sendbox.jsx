import { FormControl, Grid, IconButton, TextField, Box } from "@mui/material";
import SendIcon from '@mui/icons-material/Send';
import { Fragment, useEffect, useRef, useState } from "react";

export default function SandBox({ ws, chatroom }) {

    const sendMessage = (event) => {
        event.preventDefault();
        const data = new FormData(event.currentTarget);
        console.log('send data', data.get('message'))
        ws.emit(chatroom, data.get('message'))
    };


    return (
        <form onSubmit={sendMessage}>
            <Grid
                container spacing={1}
                alignItems="center"
                sx={{
                    height: '5rem',
                }}
            >
                <Grid xs={11} item>
                    <TextField
                        fullWidth
                        name="message"
                        label="Type your message..."
                        variant="outlined" />
                </Grid>
                <Grid xs={1} item>
                    <IconButton
                        type="submit"
                        aria-label="send"
                        color="primary">
                        <SendIcon />
                    </IconButton>
                </Grid> 
            </Grid>
        </form>
    );
}
